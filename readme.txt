Создать компонент PostsList в папке components и внутри него вывести посты полученные с энд поинта https://jsonplaceholder.typicode.com/posts, поля постов: title, body. Верстка, стили на твой вкус и цвет:)
Все полученные посты у тебя будут храниться в глобальном store Redux
P.S.: Необходимо сделать по аналогии с занятием: actions, reducer, selectors, все на своем месте и все отдельно. В компоненте PostsList диспатчим action, и с помощью selector выдергиваем посты из store
